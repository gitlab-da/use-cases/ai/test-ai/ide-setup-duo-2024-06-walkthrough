# IDE Setup Duo

This is a walkthrough of a GitLab Duo setup with VS Code, and IntelliJ for Java projects.

Created by @dnsmichi on 2024-06-11. **Not maintained. Please refer to the official documentation for the single source of truth.**


## Global Requirements

1. Accounts are provisioned based on the participants, and got a GitLab Duo seat assigned.
   1. Tip for debugging: If the personal access token throws permission errors, it might be due to wrong username/assign Duo seats. Check with your GitLab workshop instructor. 

## VS Code with GitLab Duo

https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/index.html#configure-the-extension

Preperations

1. Create a personal access token. Scope: `api` and `write_repository` 

Add a GitLab account in VS Code 

1. `cmd shift f` to open the command palette
2. Add a new GitLab account. URL: `https://gitlab.com` (leave default, press enter) Personal Access Token: Paste access token.

![vscode_command_palette_add_gitlab_account.png](vscode_command_palette_add_gitlab_account.png)

![vscode_command_palette_add_gitlab_account_success.png](vscode_command_palette_add_gitlab_account_success.png)

GitLab Workflow extension settings for Duo:

1. GitLab \> AI assisted Code Suggestions: Enabled
2. GitLab \> Duo Chat: Enabled

![vscode_gitlab_workflow_duo_settings.png](vscode_gitlab_workflow_duo_settings.png)

Duo Chat, and Code Suggestions working.

![vscode_duo_setup_success.png](vscode_duo_setup_success.png)

### Troubleshooting

https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html

cmd shift p - logs

![vscode_gitlab_extension_logs.png](vscode_gitlab_extension_logs.png)


#### Stack traces in logs

Add logs and stacktraces to support requests / into bug report issues in https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues 

## IntelliJ with GitLab Duo

Follow the documentation in https://plugins.jetbrains.com/plugin/22325-gitlab-duo and https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-editor-extensions 

### Preparations

1. Navigate into your user profile on GitLab.com
2. Create a personal access token with the permission scopes: `api`, `read_user`
3. Save the token somewhere safe (e.g. 1Password)

![intellij_duo_setup_01_gitlab_pat](intellij_duo_setup_01_gitlab_pat.png)

### IntelliJ setup

1. Download/install IntelliJ
2. Install the GitLab Duo plugin from the marketplace https://plugins.jetbrains.com/plugin/22325-gitlab-duo

![intellij_duo_setup_02_install_duo_plugin](intellij_duo_setup_02_install_duo_plugin.png)

3. Navigate into `Settings > GitLab Duo`
4. URL to GitLab instance: `https://gitlab.com` as default.
5. GitLab Personal Access Token: Paste the previously created token.

![intellij_duo_setup_03_configure_duo_plugin](intellij_duo_setup_03_configure_duo_plugin.png)

6. Save/Apply. It might take some seconds depending on the connection.
7. Click on `Verify setup` to run health checks.

![intellij_duo_setup_05_verify_setup](intellij_duo_setup_05_verify_setup.png)

### GitLab Duo access

1. Duo Chat is accessible on the right side with the Duo icon.

![intellij_duo_setup_04_access_duo_chat](intellij_duo_setup_04_access_duo_chat.png)

2. Duo Code Suggestions will be triggered inline in the code editor. Add a code comment saying `// Print Hello GitLab Duo` and wait for a suggestion.

![intellij_duo_setup_06_duo_code_suggestion_triggered](intellij_duo_setup_06_duo_code_suggestion_triggered.png)

### Troubleshooting

#### IDE log file

Navigate to `Help > Show Log in Finder`.

![intellij_duo_setup_07_troubleshooting_log](intellij_duo_setup_07_troubleshooting_log.png)

Use `cmd i` on the file to extract its path.

#### Terminal log follow

Open a terminal. Open the IntelliJ log file, and use shift+f to follow the file stream (it is basically is tail -f but you can stop the scrolling with ctrl+c and still search in the file using `/searchterm` ).

```
less ~/Library/Logs/JetBrains/IntelliJIdea2024.1/idea.log 

shift + f 
```

Navigate into IntelliJ and add a comment that will trigger Code Suggestions.

#### Stack traces in logs

Add them to all support requests / bug report issues in https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues
